﻿using System.Windows;
using System.Windows.Media.Imaging;

namespace Cybertheologians.Utility.Images
{
	public static class BitmapImageUtility
	{
		/// <summary>
		/// Sets a BitmapImage's DecodePixelHeight or Width to the specified decode pixel size,
		/// which ever value s larger. Can be used with images before or after calling EndInit().
		/// </summary>
		/// <param name="image">The BitmapImage object.</param>
		/// <param name="decodePixelSize">The nuber of pixels you want to set to the image's decode pixel size.</param>
		public static void SetDecodePixelSize(BitmapImage image, int decodePixelSize)
		{
			Int32Rect sourceRectangle = image.SourceRect;
			if (sourceRectangle.Height > sourceRectangle.Width)
				image.DecodePixelHeight = decodePixelSize;
			else
				image.DecodePixelWidth = decodePixelSize;
		}
	}
}

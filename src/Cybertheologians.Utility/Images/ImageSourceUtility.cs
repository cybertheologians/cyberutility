﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using NLog;

namespace Cybertheologians.Utility.Images
{
	public static class ImageSourceUtility
	{
		public static ImageSource ExtractSingleImageSource(Panel wrapper)
		{
			var sources = ExtractImageSources(wrapper).ToArray();
			if (sources.Count() > 1)
				throw new ArgumentException("More than one ImageSource found; only one expected.", "wrapper");

			return sources[0];
		}
		
		public static IEnumerable<ImageSource> ExtractImageSources(Panel wrapper)
		{
			s_logger.Info("Extracting image sources from Panel.");
			var children = wrapper.Children.OfType<Image>().Select(x => ((Image)x).Source);
			s_logger.Info(string.Format("Found {0} children in Panel.", children.Count()));
			return children;
		}

		public static IEnumerable<string> ConvertImageSourcesToPaths(this IEnumerable<ImageSource> sources)
		{
			s_logger.Info("Converting image sources to paths.");
			var paths = sources.Select(x => new Uri(x.ToString()).LocalPath);
			s_logger.Info(string.Format("{0} image sources converted to paths.", paths));
			return paths;
		}

		static readonly Logger s_logger = LogManager.GetCurrentClassLogger();
	}
}

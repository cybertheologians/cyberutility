﻿using System.IO;

namespace Cybertheologians.Utility
{
	public static class FileUtility
	{
		public static string GetTrimmedInvariantExtension(string path)
		{
			string extension = Path.GetExtension(path);
			extension = extension.TrimStart('.');
			return extension.ToLowerInvariant();
		}
	}
}
